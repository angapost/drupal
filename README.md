# Dockerized legacy Drupal7 apps on PHP-8.2.7 #

Resources for building [**bangapost/drupal**](https://hub.docker.com/r/bangapost/drupal) image registered publicly in Docker Hub.

Provided Dockerfile is based on official *php:8.2.7-apache-bookwarm* image 
customized with all PHP extensions required for Drupal7 plus a few more
that are useful in a corporate environment:

* ldap
* soap
* ssh2
* redis
* opcache (instead of older apc)
* XDebug 3.2.1

*Composer 2.5.8* is installed at the time of building. This can be used along with a Composer *project
template* for Drupal7 such as https://github.com/drupal-composer/drupal-project (see 7.x branch)
to help in installing Drupal core, modules, themes and performing updates for these in your own
images built on top of this.

### Why not an official Drupal image? ###

* Installs extra extensions as stated above beyond *gd*, *opcache*, *pdo_mysql*, *pdo_pgsql* and *zip*
* Installs Drupal 7 from a tarball, whereas we favour a composer workflow for managing Drupal 
* Installs Drupal, but you get more control by actually *not installing vanilla Drupal 7!*

Last point above is an important one: we believe that using composer to install/update
your custom Drupal-based application with all its contrib and custom modules in your own
custom dockerfile within your app's git repo is more maintainable!

### Tags ###

**php8.2-xdbg3-composer** latest PHP 8.2 version supported by Drupal 7.

**php5.6-xdbg2-composer** as it represents last EOLed PHP 5 version
where non-migrated legacy Drupal 7 apps are probably running.

There is also a slightly older tag **php5.6-xdbg-composer** that is essentially the same minus
the improvements in commit [XDebug config: provide remote_host](https://bitbucket.org/angapost/drupal/commits/d0f3d6edcc790fc01552350452aa9c5803c42fc0).