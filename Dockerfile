# syntax=docker/dockerfile:1
FROM php:8.2.7-apache-bookworm
MAINTAINER bangapost

# See https://github.com/docker-library/php/issues/75#issuecomment-235773906 (older base image)
# See https://github.com/mlocati/docker-php-extension-installer/blob/master/install-php-extensions
RUN DEBIAN_FRONTEND=noninteractive \
	apt-get update && apt-get install -y --no-install-recommends \
	libpng-dev libjpeg62-turbo-dev libwebp-dev libxpm-dev libfreetype6-dev \
	libmagickwand-dev imagemagick \
	libpq-dev \
	libxml2-dev \
	libldap2-dev \
	libzip-dev zip \
	libssh2-1-dev libssh2-1 \
	&& docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
	&& docker-php-ext-install ldap \
	&& pecl channel-update pecl.php.net && pecl install imagick \
	&& docker-php-ext-enable imagick \
	&& docker-php-ext-configure gd --with-jpeg --with-webp --with-xpm --with-freetype \
	&& docker-php-ext-install gd \
	pdo_mysql \
	pdo_pgsql \
	&& docker-php-ext-install zip \
	opcache \
	bcmath \
	soap \
	&& pecl install redis-5.3.7 \
	&& docker-php-ext-enable redis \
	&& pecl install ssh2 \
	&& docker-php-ext-enable ssh2 \
	&& pecl install xdebug-3.2.1 \
	&& mv $PHP_INI_DIR/php.ini-development $PHP_INI_DIR/php.ini \
	&& echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > $PHP_INI_DIR/conf.d/xdebug.ini \
	&& echo "xdebug.mode=debug" >> $PHP_INI_DIR/conf.d/xdebug.ini \
	&& echo "xdebug.start_with_request=no" >> $PHP_INI_DIR/conf.d/xdebug.ini \
	&& echo "xdebug.client_host=HOST.DOCKER.INTERNAL" >> $PHP_INI_DIR/conf.d/xdebug.ini \
	&& echo ";xdebug.log=/tmp/xdebug.log" >> $PHP_INI_DIR/conf.d/xdebug.ini \
	&& echo "allow_url_fopen = On" > $PHP_INI_DIR/conf.d/drupal-01.ini \
	&& curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer \
	&& a2enmod headers rewrite \
	&& rm -rf /var/lib/apt/lists/* \
	/tmp/* \
	/usr/share/doc/* \
	/usr/src \
	/var/cache/* \
	/var/tmp/*

COPY drupal-*.ini $PHP_INI_DIR/conf.d/

RUN mkdir -p /tmp/drupal \
	&& chown www-data:www-data /tmp/drupal \
	&& chown www-data:www-data /var/www

WORKDIR /var/www/drupal

ADD apache.conf /etc/apache2/sites-enabled/000-default.conf
ADD drushrc.php /etc/drush/drushrc.php

ENV DOCROOT=/var/www/drupal/web